FROM alpine:latest

MAINTAINER ylqjgm ylqjgm@gmail.com

COPY conf /conf

COPY static /static

COPY views /views

COPY back /back

COPY qshell /qshell

COPY SCBlog /SCBlog

COPY run.sh /run.sh

RUN apk --update add bash;\
chmod +x /SCBlog;\
chmod +x /qshell;\
chmod +x /back;\
chmod +x /run.sh

VOLUME ["/static/upload"]

EXPOSE 80

CMD ["/bin/bash", "/run.sh"]

