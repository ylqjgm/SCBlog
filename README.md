# Golang语言编写到博客程序

作者博客：<http://www.lovekk.org>

# 程序说明

本程序使用Golang开发，为作者自用博客程序

# 安装说明

* 安装Golang
* 安装Git
* 安装MongoDB

## 获取所使用的第三方库

```bash
go get github.com/astaxie/beego
go get github.com/axgle/mahonia
go get gopkg.in/mgo.v2
go get github.com/shirou/gopsutil
```

## 配置conf/app.conf

```
appname = SCBlog
httpport = 80
runmode = pro
sessionon = true

dbhost = 127.0.0.1
dbport = 27017
dbname = SCBlog
dbuser =
dbpass =
adminuser = lovekk
adminpass = www.lovekk.org
```

## 编译运行

进入SCBlog源码目录，运行如下命令

```bash
go build
./SCBlog
```

# Docker方式安装

## 安装Docker

具体安装方法请参考Google

## 编译源码

进入SCBlog源码目录，编译SCBlog

```bash
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o SCBlog .
```

进入SCBlog源码目录，编译back

```bash
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o back back.go
```

## 配置qupload.conf

qupload.conf是为了自动备份数据到七牛中

```
{
    "src_dir"		:	"/backup/",
    "access_key"	:	"<Your Access Key>",
    "secret_key"	:	"<Your Secret Key>",
    "up_host"		:	"http://up.qiniu.com",
    "bucket"		:	"<Your Bucket>",
    "ignore_dir"	:	true,
    "check_exists"	:	true
}
```

## 配置conf/app.conf

```
appname = SCBlog
httpport = 80
runmode = pro
sessionon = true

dbhost = 127.0.0.1
dbport = 27017
dbname = SCBlog
dbuser =
dbpass =
adminuser = lovekk
adminpass = www.lovekk.org
```

## 创建Docker镜像

```bash
docker build -t ylqjgm/scblog:<tag> .
```

## 上传并启动

依据各平台提供的信息将镜像上传到平台，并在后台创建启动

```bash
docker tag <images id> <docker url>/ylqjgm/scblog:<tag>
docker push <docker url>/ylqjgm/scblog:<tag>
```

# 授权方式

本程序遵循MIT授权

# 程序反馈

<http://www.lovekk.org>
