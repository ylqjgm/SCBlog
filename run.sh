#!/bin/bash

MONGODB_HOST=${MONGODB_HOST:-"127.0.0.1"}
MONGODB_PORT=${MONGODB_PORT:-"27017"}
MONGODB_NAME=${MONGODB_NAME:-"SCDht"}
MONGODB_USER=${MONGODB_USER:-""}
MONGODB_PASS=${MONGODB_PASS:-""}
ADMIN_USER=${ADMIN_USER:-"lovekk"}
ADMIN_PASS=${ADMIN_PASS:-"www.lovekk.org"}
ACCESS_KEY=${ACCESS_KEY:-""}
SECRET_KEY=${SECRET_KEY:-""}
BUCKET=${BUCKET:-""}

cat << EOF > /conf/app.conf
appname = SCBlog
httpport = 80
runmode = pro
sessionon = true

dbhost = $MONGODB_HOST
dbport = $MONGODB_PORT
dbname = $MONGODB_NAME
dbuser = $MONGODB_USER
dbpass = $MONGODB_PASS
adminuser = $ADMIN_USER
adminpass = $ADMIN_PASS
EOF

cat << EOF > /qupload.conf
{
    "src_dir"       :   "/backup/",
    "access_key"    :   "$ACCESS_KEY",
    "secret_key"    :   "$SECRET_KEY",
    "up_host"       :   "http://up.qiniu.com",
    "bucket"        :   "$BUCKET",
    "ignore_dir"    :   true,
    "check_exists"  :   true
}
EOF

/back &

/SCBlog
