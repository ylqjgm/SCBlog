package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"
	"unicode/utf8"

	"github.com/robfig/cron"
	"github.com/ylqjgm/SCBlog/models"
)

var hex = "0123456789abcdef"

func filter(s string) string {
	var e bytes.Buffer
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if 0x20 <= b && b != '\\' && b != '"' && b != '<' && b != '>' && b != '&' {
				i++
				continue
			}
			if start < i {
				e.WriteString(s[start:i])
			}
			switch b {
			case '\\', '"':
				e.WriteByte('\\')
				e.WriteByte(b)
			case '\n':
				e.WriteByte('\\')
				e.WriteByte('n')
			case '\r':
				e.WriteByte('\\')
				e.WriteByte('r')
			case '\t':
				e.WriteByte('\\')
				e.WriteByte('t')
			default:
				e.WriteString(`\u00`)
				e.WriteByte(hex[b>>4])
				e.WriteByte(hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRuneInString(s[i:])
		if c == utf8.RuneError && size == 1 {
			if start < i {
				e.WriteString(s[start:i])
			}
			e.WriteString(`\ufffd`)
			i += size
			start = i
			continue
		}
		if c == '\u2028' || c == '\u2029' {
			if start < i {
				e.WriteString(s[start:i])
			}
			e.WriteString(`\u202`)
			e.WriteByte(hex[c&0xF])
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		e.WriteString(s[start:])
	}

	return string(e.Bytes())
}

func Back() {
	os.MkdirAll("/backup/", 0777)

	err := compress("/static/upload/", "/backup/Web_"+time.Now().Format("20060102")+".zip")
	if err != nil {
		log.Fatalln(err.Error())
	}

	os.MkdirAll("/backup/data/", 0777)
	var posts []models.SC_Post
	models.GetAllByQuery(models.DbPost, nil, &posts)
	str := ""
	for _, post := range posts {
		var html, markdown bytes.Buffer
		json.HTMLEscape(&html, []byte(filter(post.Html)))
		json.HTMLEscape(&markdown, []byte(filter(post.Markdown)))
		str += `{"_id":{"$oid":"` + post.Id.Hex() + `"},"caption":"` + post.Caption + `","cover":"` + post.Cover + `","created":{"$numberLong":"` + strconv.FormatInt(post.Created, 10) + `"},"html":"` + string(html.Bytes()) + `","markdown":"` + string(markdown.Bytes()) + `","slug":"` + post.Slug + `","tags":[`
		for _, tt := range post.Tags {
			str += `"` + tt + `",`
		}
		str = str[:len(str)-1]

		str += `],"type":"` + post.Type + `"}`

		str += "\n"
	}

	err = ioutil.WriteFile("/backup/data/SC_Post.json", []byte(str), 0666)
	if err != nil {
		log.Fatalln(err.Error())
	}

	var redirects []models.SC_Redirect
	models.GetAllByQuery(models.DbRedirect, nil, &redirects)
	str = ""
	for _, redirect := range redirects {
		str += `{"_id":{"$oid":"` + redirect.Id.Hex() + `"},"caption":"` + redirect.Caption + `","link":"` + redirect.Link + `"}`
		str += "\n"
	}

	err = ioutil.WriteFile("/backup/data/SC_Redirect.json", []byte(str), 0666)
	if err != nil {
		log.Fatalln(err.Error())
	}

	var tags []models.SC_Tag
	models.GetAllByQuery(models.DbTag, nil, &tags)
	str = ""
	for _, tag := range tags {
		str += `{"_id":{"$oid":"` + tag.Id.Hex() + `"},"caption":"` + tag.Caption + `","slug":"` + tag.Slug + `"}`
		str += "\n"
	}

	err = ioutil.WriteFile("/backup/data/SC_Tag.json", []byte(str), 0666)
	if err != nil {
		log.Fatalln(err.Error())
	}

	var configs []models.SC_Config
	models.GetAllByQuery(models.DbConf, nil, &configs)
	str = ""
	for _, conf := range configs {
		str += `{"_id":{"$oid":"` + conf.Id.Hex() + `"},"setkey":"` + conf.SetKey + `","setval":"` + conf.SetVal + `"}`
		str += "\n"
	}

	err = ioutil.WriteFile("/backup/data/SC_Config.json", []byte(str), 0666)
	if err != nil {
		log.Fatalln(err.Error())
	}

	err = compress("/backup/data/", "/backup/Data_"+time.Now().Format("20060102")+".zip")
	if err != nil {
		log.Fatalln(err.Error())
	}

	os.RemoveAll("/backup/data/")

	upload()
}

func upload() {
	// 创建命令
	cmd := exec.Command("/qshell", "qupload", "10", "/qupload.conf")
	var out bytes.Buffer
	var stderr bytes.Buffer
	// 输出
	cmd.Stdout = &out
	// 输入
	cmd.Stderr = &stderr
	// 执行同步
	err := cmd.Run()
	if err != nil {
		println(out.String())
		println(stderr.String())
	}
}

// 参数frm可以是文件或目录，不会给dst添加.zip扩展名
func compress(frm, dst string) error {
	buf := bytes.NewBuffer(make([]byte, 0, 10*1024*1024)) // 创建一个读写缓冲
	myzip := zip.NewWriter(buf)                           // 用压缩器包装该缓冲
	// 用Walk方法来将所有目录下的文件写入zip
	err := filepath.Walk(frm, func(path string, info os.FileInfo, err error) error {
		var file []byte
		if err != nil {
			return filepath.SkipDir
		}
		header, err := zip.FileInfoHeader(info) // 转换为zip格式的文件信息
		if err != nil {
			return filepath.SkipDir
		}
		header.Name, _ = filepath.Rel(filepath.Dir(frm), path)
		if !info.IsDir() {
			// 确定采用的压缩算法（这个是内建注册的deflate）
			header.Method = 8
			file, err = ioutil.ReadFile(path) // 获取文件内容
			if err != nil {
				return filepath.SkipDir
			}
		} else {
			file = nil
		}
		// 上面的部分如果出错都返回filepath.SkipDir
		// 下面的部分如果出错都直接返回该错误
		// 目的是尽可能的压缩目录下的文件，同时保证zip文件格式正确
		w, err := myzip.CreateHeader(header) // 创建一条记录并写入文件信息
		if err != nil {
			return err
		}
		_, err = w.Write(file) // 非目录文件会写入数据，目录不会写入数据
		if err != nil {        // 因为目录的内容可能会修改
			return err // 最关键的是我不知道咋获得目录文件的内容
		}
		return nil
	})
	if err != nil {
		return err
	}
	myzip.Close()               // 关闭压缩器，让压缩器缓冲中的数据写入buf
	file, err := os.Create(dst) // 建立zip文件
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = buf.WriteTo(file) // 将buf中的数据写入文件
	if err != nil {
		return err
	}
	return nil
}

// 主函数
func main() {
	// 新建任务
	c := cron.New()
	// 添加每天任务
	c.AddFunc("@daily", func() { Back() })
	// 启动任务
	c.Start()

	// 阻塞主线程不退出
	select {}
}

