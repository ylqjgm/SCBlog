package common

import (
	"crypto/md5"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/ylqjgm/SCBlog/models"
	"gopkg.in/mgo.v2/bson"
)

// 生成Gravatar头像URL
func Gravatar(email string, size int) string {
	// 如果大小小于1
	if size < 1 {
		// 设置大小为80
		size = 80
	}

	// 将email地址去除空格并转换为小写
	email = strings.ToLower(strings.TrimSpace(email))
	// 创建一个MD5
	hash := md5.New()
	// 将email转换为MD5
	hash.Write([]byte(email))

	// 返回生成的Gravatar头像URL
	return fmt.Sprintf("https://secure.gravatar.com/avatar/%x?s=%d", hash.Sum(nil), size)
}

// 过滤Html
func filterHtml(str string) string {
	// 将Html标签全部转换为小写
	re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
	str = re.ReplaceAllStringFunc(str, strings.ToLower)

	// 去除Style
	re, _ = regexp.Compile("\\<style[\\S\\s]+?\\</style\\>")
	str = re.ReplaceAllString(str, "")

	// 去除Script
	re, _ = regexp.Compile("\\<script[\\S\\s]+?\\</script\\>")
	str = re.ReplaceAllString(str, "")

	// 去除所有尖括号内的Html代码, 并换成换行符
	re, _ = regexp.Compile("\\<[\\S\\s]+?\\>")
	str = re.ReplaceAllString(str, "\n")

	// 去除连续的换行符
	re, _ = regexp.Compile("\\S\\s{2,}")
	str = re.ReplaceAllString(str, "\n")

	return str
}

// 获取预览内容
func Preview(str string, length int) string {
	// 先过滤Html
	str = filterHtml(str)
	// 将字符串转换为rune列表
	rs := []rune(str)
	// 获取长度
	rl := len(rs)

	// 如果截取长度大于字符串长度
	if length > rl {
		// 截取长度等于字符串长度
		str = string(rs[0:rl])
	} else {
		str = string(rs[0:length]) + "..."
	}

	return strings.Replace(str, "\n", "", -1)
}

// 获取Id
func GetId(id bson.ObjectId) string {
	return id.Hex()
}

// 获取Tag列表
func GetTagSlug(caption string) string {
	var tag models.SC_Tag

	models.GetOneByQuery(models.DbTag, bson.M{"caption": caption}, &tag)

	return tag.Slug
}

// 自动获取slug
func GetSlug(str string, isslug bool) string {
	retstr := ""

	// 循环转换全角为半角
	for _, i := range str {
		inside_code := i
		if inside_code == 12288 {
			inside_code = 32
		} else {
			inside_code -= 65248
		}
		if inside_code < 32 || inside_code > 126 {
			retstr += string(i)
		} else {
			retstr += string(inside_code)
		}
	}

	// 定义去除标点正则
	reg := regexp.MustCompile(`[\pP]+`)
	// 替换所有标点符号为空
	str = reg.ReplaceAllString(retstr, "")
	// 去除首尾空格
	str = strings.TrimSpace(str)
	// 定义去除空格正则
	reg = regexp.MustCompile(`[\sS]+`)
	// 替换所有连续空格为单空格
	str = reg.ReplaceAllString(retstr, "")
	// 将空格更换为-
	str = strings.Replace(str, " ", "-", -1)

	// 返回转换后的字符串
	return str
}

// 获取运行时间
func LoadTimes(startTime time.Time) string {
	return fmt.Sprintf("%dms", time.Now().Sub(startTime)/1000000)
}

// 转换字节为对应大小
func ToSize(size uint64) string {
	switch {
	case size < 1024:
		return fmt.Sprintf("%d B", size)
	case size < 1024*1024 && size >= 1024:
		return fmt.Sprintf("%d KB", size/1024)
	case size < 1024*1024*1024 && size >= 1024*1024:
		return fmt.Sprintf("%d MB", size/1024/1024)
	case size < 1024*1024*1024*1024 && size >= 1024*1024*1024:
		return fmt.Sprintf("%d GB", size/1024/1024/1024)
	case size < 1024*1024*1024*1024*1024 && size >= 1024*1024*1024*1024:
		return fmt.Sprintf("%d TB", size/1024/1024/1024/1024)
	}

	return ""
}

func DateFormat(tm int64) string {
	tt := time.Unix(tm, 0)
	return tt.Format("2006-01-02T15:04:05+07:00")
}
